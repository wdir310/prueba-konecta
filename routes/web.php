<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//administradores

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/editar/{id}', 'HomeController@editar' )->name('editar');
Route::put('/editar/{id}', 'HomeController@update' )->name('update');
Route::delete('/eliminar/{id}', 'HomeController@eliminar')->name('eliminar');

//vendedor

Route::get('/registerCliente', 'ClientController@registerCliente')->name('registerCliente');
Route::post('/storeCliente', 'ClientController@storeCliente')->name('storeCliente');
Route::get('/listCliente', 'ClientController@listCliente')->name('listCliente');
Route::get('/editarCliente/{id}', 'ClientController@editarCliente' )->name('editarCliente');
Route::put('/editarCliente/{id}', 'ClientController@updateCliente' )->name('updateCliente');
Route::delete('/eliminarCliente/{id}', 'ClientController@eliminarCliente')->name('eliminarCliente');