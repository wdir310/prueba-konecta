<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin' ,
            'Documento' => '123456',
            'Direccion' => 'Direccion',
            'email' => 'admin@gmail.com',
            'type' => 'administrador',
            'password' => bcrypt('Admin123'),
        ]);
    }
}
