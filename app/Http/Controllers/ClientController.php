<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App;

class ClientController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listCliente()
    {
        $client = DB::table('client')->paginate(5);
        
        return view('listCliente', ['client' => $client]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerCliente()
    {
        return view('registerCliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCliente(Request $request)
    {
        $cliente = new App\client;
        $cliente->name = $request->name;
        $cliente->Documento = $request->Documento;
        $cliente->Direccion = $request->Direccion;
        $cliente->email = $request->email;

        $cliente->save();

        return redirect()->route('listCliente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editarCliente($id)
    {
        $data = App\client::findOrFail($id);
        return view('editarCliente', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCliente(Request $request, $id)
    {
        $clienteActualizado = App\client::find($id);
        $clienteActualizado->name = $request->name;
        $clienteActualizado->Documento = $request->Documento;
        $clienteActualizado->Direccion = $request->Direccion;
        $clienteActualizado->email = $request->email;
        $clienteActualizado->save();
        return redirect()->route('listCliente')->with('mensaje', 'Cliente editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminarCliente($id)
    {
        $clienteEliminar = App\client::findOrFail($id);
        $clienteEliminar->delete();
    
        return redirect()->route('listCliente')->with('mensaje', 'Cliente Eliminado');
    }
}
