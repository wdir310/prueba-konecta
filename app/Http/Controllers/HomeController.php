<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = DB::table('users')->paginate(3);
        
        return view('/home', ['users' => $users]);
    }

    public function editar($id)
    {
        $data = App\User::findOrFail($id);
        return view('editar', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $userActualizada = App\User::find($id);
        $userActualizada->name = $request->name;
        $userActualizada->Documento = $request->Documento;
        $userActualizada->Direccion = $request->Direccion;
        $userActualizada->email = $request->email;
        $userActualizada->type = $request->type;
        $userActualizada->save();
        return redirect()->route('home')->with('mensaje', 'Usuario editado!');
    }

    public function eliminar($id){

        $userEliminar = App\User::findOrFail($id);
        $userEliminar->delete();
    
        return redirect()->route('home')->with('mensaje', 'Usuario Eliminado');
    }

   
}

