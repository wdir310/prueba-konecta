@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h1 class="display-6 text-center">Listado de Usuarios</h1>
                <a href="{{ route('listCliente') }}" class="btn btn-outline-secondary">Administrar Clientes</a>
                <a href="{{ route('register') }}" class="btn btn-outline-info">Nuevo Registro</a>
                </div>
                
                    <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('mensaje'))
                        <div class="alert alert-success">
                            {{ session('mensaje') }}
                        </div>
                    @endif
                        <div class="container my-4">
                            
                            <table class="table">
                                <thead class="table-info">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Documento</th>
                                        <th scope="col">Direccion</th>
                                        <th scope="col">Correo</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $item)
                                    <tr>
                                        <th scope="row">{{ $item->id }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->Documento }}</td>
                                        <td>{{ $item->Direccion }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                            @if ($item->type == "administrador")
                                            <span class="badge badge-danger">{{ $item->type }}</span>
                                            @else
                                            <span class="badge badge-primary">{{ $item->type }}</span>
                                            @endif
                                         </td>
                                        <td><a href="{{ route('editar', $item->id) }}" class="btn btn-outline-success btn-sm">Editar</a>
                                        <form action="{{ route('eliminar', $item->id) }}" class="d-inline" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                                        </form> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                                {{ $users->links() }}
                        </div>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
